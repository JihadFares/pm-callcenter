//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CallCenterDashboard1.EDMX
{
    using System;
    
    public partial class GetEvaluation_Result
    {
        public Nullable<double> daira_code { get; set; }
        public Nullable<double> evaluation_code { get; set; }
        public string evaluation_desc { get; set; }
    }
}
