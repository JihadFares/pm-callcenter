﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Newtonsoft.Json;
using CallCenterDashboard1.Controllers;

namespace CallCenterDashboard1.Globals
{
    public static class Configuration
    {
        public static string APPLICATION_TITLE = "Call Center";
        public static string APPLICATION_CLIENT = "PMS";

        public static string APPLICATION_FILE_UPLOAD_STORAGE_PATH = @"C:\Temp";

        public static string DATA_INTERFACE_URL = "http://pmdev.wbintl.co:1682/";
        //public static string DATA_INTERFACE_URL = "http://localhost:45122/";
    }
}