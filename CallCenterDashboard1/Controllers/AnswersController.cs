﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallCenterDashboard1.EDMX;

namespace CallCenterDashboard1.Controllers
{
    public class AnswersController : Controller
    {
        private PMCallCenterEntities db = new PMCallCenterEntities();

        // GET: Answers
        public ActionResult Index()
        {
            var answers = db.Answers.Include(a => a.AnswerTemplate).Include(a => a.Question);
            return View(answers.ToList());
        }

        // GET: Answers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = db.Answers.Find(id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            return View(answer);
        }

        // GET: Answers/Create
        public ActionResult Create()
        {
            ViewBag.AnswerTemplateKey = new SelectList(db.AnswerTemplates, "AnswerTemplateKey", "Value");
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text");
            return View();
        }

        // POST: Answers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AnswerKey,QuestionKey,AnswerTemplateKey,IndividualKey")] Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Answers.Add(answer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AnswerTemplateKey = new SelectList(db.AnswerTemplates, "AnswerTemplateKey", "Value", answer.AnswerTemplateKey);
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text", answer.QuestionKey);
            return View(answer);
        }

        // GET: Answers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = db.Answers.Find(id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            ViewBag.AnswerTemplateKey = new SelectList(db.AnswerTemplates, "AnswerTemplateKey", "Value", answer.AnswerTemplateKey);
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text", answer.QuestionKey);
            return View(answer);
        }

        // POST: Answers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AnswerKey,QuestionKey,AnswerTemplateKey,IndividualKey")] Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(answer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AnswerTemplateKey = new SelectList(db.AnswerTemplates, "AnswerTemplateKey", "Value", answer.AnswerTemplateKey);
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text", answer.QuestionKey);
            return View(answer);
        }

        // GET: Answers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Answer answer = db.Answers.Find(id);
            if (answer == null)
            {
                return HttpNotFound();
            }
            return View(answer);
        }

        // POST: Answers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Answer answer = db.Answers.Find(id);
            db.Answers.Remove(answer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
