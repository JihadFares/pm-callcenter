﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallCenterDashboard1.EDMX;

namespace CallCenterDashboard1.Controllers
{
    public class OperationsController : Controller
    {
        private PMCallCenterEntities db = new PMCallCenterEntities();

        // GET: Operations
        public ActionResult Index()
        {
            var operations = db.Operations.Include(o => o.OperationStatu).Include(o => o.Script).Include(o => o.Survey);
            return View(operations.ToList());
        }

        // GET: Operations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Operation operation = db.Operations.Find(id);
            if (operation == null)
            {
                return HttpNotFound();
            }
            return View(operation);
        }

        // GET: Operations/Create
        public ActionResult Create()
        {
            ViewBag.OperationStatusKey = new SelectList(db.OperationStatus, "OperationStatusKey", "Value");
            ViewBag.ScriptKey = new SelectList(db.Scripts, "ScriptKey", "Text");
            ViewBag.SurveyKey = new SelectList(db.Surveys, "SurveyKey", "Description");
            return View();
        }

        // POST: Operations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OperationKey,Name,Note,Alias,StartDate,EndDate,ScriptKey,OperationStatusKey,SurveyKey,OperationLogo")] Operation operation)
        {
            if (ModelState.IsValid)
            {
                db.Operations.Add(operation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OperationStatusKey = new SelectList(db.OperationStatus, "OperationStatusKey", "Value", operation.OperationStatusKey);
            ViewBag.ScriptKey = new SelectList(db.Scripts, "ScriptKey", "Text", operation.ScriptKey);
            ViewBag.SurveyKey = new SelectList(db.Surveys, "SurveyKey", "Description", operation.SurveyKey);
            return View(operation);
        }

        // GET: Operations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Operation operation = db.Operations.Find(id);
            if (operation == null)
            {
                return HttpNotFound();
            }
            ViewBag.OperationStatusKey = new SelectList(db.OperationStatus, "OperationStatusKey", "Value", operation.OperationStatusKey);
            ViewBag.ScriptKey = new SelectList(db.Scripts, "ScriptKey", "Text", operation.ScriptKey);
            ViewBag.SurveyKey = new SelectList(db.Surveys, "SurveyKey", "Description", operation.SurveyKey);
            return View(operation);
        }

        // POST: Operations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OperationKey,Name,Note,Alias,StartDate,EndDate,ScriptKey,OperationStatusKey,SurveyKey,OperationLogo")] Operation operation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(operation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OperationStatusKey = new SelectList(db.OperationStatus, "OperationStatusKey", "Value", operation.OperationStatusKey);
            ViewBag.ScriptKey = new SelectList(db.Scripts, "ScriptKey", "Text", operation.ScriptKey);
            ViewBag.SurveyKey = new SelectList(db.Surveys, "SurveyKey", "Description", operation.SurveyKey);
            return View(operation);
        }

        // GET: Operations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Operation operation = db.Operations.Find(id);
            if (operation == null)
            {
                return HttpNotFound();
            }
            return View(operation);
        }

        // POST: Operations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Operation operation = db.Operations.Find(id);
            db.Operations.Remove(operation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
