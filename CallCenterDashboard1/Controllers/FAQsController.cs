﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallCenterDashboard1.EDMX;

namespace CallCenterDashboard1.Controllers
{
    public class FAQsController : Controller
    {
        private PMCallCenterEntities db = new PMCallCenterEntities();

        // GET: FAQs
        public ActionResult Index()
        {
            var fAQs = db.FAQs.Include(f => f.AnswerTemplate).Include(f => f.Operation).Include(f => f.Question);
            return View(fAQs.ToList());
        }

        // GET: FAQs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return HttpNotFound();
            }
            return View(fAQ);
        }

        // GET: FAQs/Create
        public ActionResult Create()
        {
            ViewBag.AnswerTemplateKey = new SelectList(db.AnswerTemplates, "AnswerTemplateKey", "Value");
            ViewBag.OperationKey = new SelectList(db.Operations, "OperationKey", "Name");
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text");
            return View();
        }

        // POST: FAQs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FAQKey,QuestionKey,AnswerTemplateKey,FAQType,OperationKey")] FAQ fAQ)
        {
            if (ModelState.IsValid)
            {
                db.FAQs.Add(fAQ);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AnswerTemplateKey = new SelectList(db.AnswerTemplates, "AnswerTemplateKey", "Value", fAQ.AnswerTemplateKey);
            ViewBag.OperationKey = new SelectList(db.Operations, "OperationKey", "Name", fAQ.OperationKey);
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text", fAQ.QuestionKey);
            return View(fAQ);
        }

        // GET: FAQs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return HttpNotFound();
            }
            ViewBag.AnswerTemplateKey = new SelectList(db.AnswerTemplates, "AnswerTemplateKey", "Value", fAQ.AnswerTemplateKey);
            ViewBag.OperationKey = new SelectList(db.Operations, "OperationKey", "Name", fAQ.OperationKey);
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text", fAQ.QuestionKey);
            return View(fAQ);
        }

        // POST: FAQs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FAQKey,QuestionKey,AnswerTemplateKey,FAQType,OperationKey")] FAQ fAQ)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fAQ).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AnswerTemplateKey = new SelectList(db.AnswerTemplates, "AnswerTemplateKey", "Value", fAQ.AnswerTemplateKey);
            ViewBag.OperationKey = new SelectList(db.Operations, "OperationKey", "Name", fAQ.OperationKey);
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text", fAQ.QuestionKey);
            return View(fAQ);
        }

        // GET: FAQs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return HttpNotFound();
            }
            return View(fAQ);
        }

        // POST: FAQs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FAQ fAQ = db.FAQs.Find(id);
            db.FAQs.Remove(fAQ);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
