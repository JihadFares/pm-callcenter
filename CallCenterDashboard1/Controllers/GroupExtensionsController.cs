﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallCenterDashboard1.EDMX;

namespace CallCenterDashboard1.Controllers
{
    public class GroupExtensionsController : Controller
    {
        private PMCallCenterEntities db = new PMCallCenterEntities();

        // GET: GroupExtensions
        public ActionResult Index()
        {
            return View(db.GroupExtensions.ToList());
        }

        // GET: GroupExtensions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GroupExtension groupExtension = db.GroupExtensions.Find(id);
            if (groupExtension == null)
            {
                return HttpNotFound();
            }
            return View(groupExtension);
        }

        // GET: GroupExtensions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GroupExtensions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GroupExtensionKey,Name,OperationKey,UserExtensionKey")] GroupExtension groupExtension)
        {
            if (ModelState.IsValid)
            {
                db.GroupExtensions.Add(groupExtension);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(groupExtension);
        }

        // GET: GroupExtensions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GroupExtension groupExtension = db.GroupExtensions.Find(id);
            if (groupExtension == null)
            {
                return HttpNotFound();
            }
            return View(groupExtension);
        }

        // POST: GroupExtensions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GroupExtensionKey,Name,OperationKey,UserExtensionKey")] GroupExtension groupExtension)
        {
            if (ModelState.IsValid)
            {
                db.Entry(groupExtension).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(groupExtension);
        }

        // GET: GroupExtensions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GroupExtension groupExtension = db.GroupExtensions.Find(id);
            if (groupExtension == null)
            {
                return HttpNotFound();
            }
            return View(groupExtension);
        }

        // POST: GroupExtensions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GroupExtension groupExtension = db.GroupExtensions.Find(id);
            db.GroupExtensions.Remove(groupExtension);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
