﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallCenterDashboard1.EDMX;

namespace CallCenterDashboard1.Controllers
{
    public class IndividualExtensionsController : Controller
    {
        private PMCallCenterEntities db = new PMCallCenterEntities();

        // GET: IndividualExtensions
        public ActionResult Index()
        {
            var individualExtensions = db.IndividualExtensions.Include(i => i.CallStatu).Include(i => i.GroupExtension);
            return View(individualExtensions.ToList());
        }

        // GET: IndividualExtensions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IndividualExtension individualExtension = db.IndividualExtensions.Find(id);
            if (individualExtension == null)
            {
                return HttpNotFound();
            }
            return View(individualExtension);
        }

        // GET: IndividualExtensions/Create
        public ActionResult Create()
        {
            ViewBag.CallStatusKey = new SelectList(db.CallStatus, "CallStatusKey", "Value");
            ViewBag.GroupKey = new SelectList(db.GroupExtensions, "GroupExtensionKey", "Name");
            return View();
        }

        // POST: IndividualExtensions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IndividualExtensionKey,IndividualKey,CallStatusKey,GroupKey,IndividualOrder")] IndividualExtension individualExtension)
        {
            if (ModelState.IsValid)
            {
                db.IndividualExtensions.Add(individualExtension);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CallStatusKey = new SelectList(db.CallStatus, "CallStatusKey", "Value", individualExtension.CallStatusKey);
            ViewBag.GroupKey = new SelectList(db.GroupExtensions, "GroupExtensionKey", "Name", individualExtension.GroupKey);
            return View(individualExtension);
        }

        // GET: IndividualExtensions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IndividualExtension individualExtension = db.IndividualExtensions.Find(id);
            if (individualExtension == null)
            {
                return HttpNotFound();
            }
            ViewBag.CallStatusKey = new SelectList(db.CallStatus, "CallStatusKey", "Value", individualExtension.CallStatusKey);
            ViewBag.GroupKey = new SelectList(db.GroupExtensions, "GroupExtensionKey", "Name", individualExtension.GroupKey);
            return View(individualExtension);
        }

        // POST: IndividualExtensions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IndividualExtensionKey,IndividualKey,CallStatusKey,GroupKey,IndividualOrder")] IndividualExtension individualExtension)
        {
            if (ModelState.IsValid)
            {
                db.Entry(individualExtension).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CallStatusKey = new SelectList(db.CallStatus, "CallStatusKey", "Value", individualExtension.CallStatusKey);
            ViewBag.GroupKey = new SelectList(db.GroupExtensions, "GroupExtensionKey", "Name", individualExtension.GroupKey);
            return View(individualExtension);
        }

        // GET: IndividualExtensions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IndividualExtension individualExtension = db.IndividualExtensions.Find(id);
            if (individualExtension == null)
            {
                return HttpNotFound();
            }
            return View(individualExtension);
        }

        // POST: IndividualExtensions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IndividualExtension individualExtension = db.IndividualExtensions.Find(id);
            db.IndividualExtensions.Remove(individualExtension);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
