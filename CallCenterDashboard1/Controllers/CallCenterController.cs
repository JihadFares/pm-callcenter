﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CallCenterDashboard1.EDMX;
using Newtonsoft.Json.Linq;

namespace CallCenterDashboard1.Controllers
{
    [Authorize]
    public class CallCenterController : Controller
    {
        // GET: CallCenter
        public ActionResult Dashboard()
        {
            return View("Dashboard");
        }


        public ActionResult UserInfo()
        {
            ViewBag.Message = "Your UserInfo page.";

            return View();
        }

        public ActionResult OperationProgress()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult OperationScript()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Clock()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Separator()
        {
            ViewBag.Message = "";

            return View();
        }

        public async Task<ActionResult> IndividualsList(string operationKey = null)
        {
            operationKey = Convert.ToString(9);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("CallCenter/getCallStatuses/");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject<List<GetIndividualsByOperation_Result>>(responseJSON);

                    //List<string> resultStatic = new List<string>();
                    //resultStatic.Add("Call Pending");
                    //resultStatic.Add("Call Back");
                    //resultStatic.Add("Wrong Number");
                    //resultStatic.Add("No Answer");
                    //resultStatic.Add("Completed");

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    ViewBag.CallStatusList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return View();
        }


        public async Task<string> fillOperationsDropDown()
        {
            string result = string.Empty;
            List<string> Result = new List<string>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getOperationsInfoByUser");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("userName={0}", User.Identity.Name);

               HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
               if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                   result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //string responseJSONInner = "[" + await response.Content.ReadAsStringAsync() + "]";
                    //var Answerscount = JArray.Parse(responseJSONInner).Children();

                    //Answerscount

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
               }
            }
             
            return result;
        }

        public async Task<string> displayOperationInformation(int operationKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getOperationsInfoByUser");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("userName={0}", User.Identity.Name);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return result;
        }

        public async Task<JsonResult> fillOverallProgressBar()
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getOverallProgress");
                //requestUriBuilder.Append("?");
                //requestUriBuilder.AppendFormat("operationKey={0}", operationKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> fillOperationProgressBar(int operationKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getOperationProgressByUser");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("userName={0}", User.Identity.Name);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //getRemainingIndividuals
        public async Task<string> fillRemainingProgressBar(int operationKey) //NOT DONE
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getRemainingIndividualsByUser");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("userName={0}", User.Identity.Name);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return result;
        }

        public async Task<string> fillIndividualsList(int operationKey, string username)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getIndividualsByUserName");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("userName={0}", username);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));
                }
            }

            return result;
        }


        public  async Task<ActionResult> PrintCall(int operationKey, string username, string operationName, string callStatus)
        {
            //int operationKey, string username
            List<JToken> individualsList = new List<JToken>();
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getIndividualsByUserName");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("userName={0}", username);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();
                    individualsList = JArray.Parse(responseJSON).Children().ToList().Where(x => x["Value"].ToString().ToLower().Trim().Replace(" ",string.Empty) == callStatus.ToLower().Trim().Replace(" ", string.Empty)).Select(x => x).ToList();
                }
            }

            ViewBag.OperationName = operationName;
            ViewBag.CallStatus = callStatus;

            return View("PrintCall",individualsList);
        }

        //getQuestionsForOperation
        public async Task<string> fillQuestionsList(int operationKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getQuestionsForOperation");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return result;
        }

        //getAnswersForQuestions
        public async Task<string> fillAnswersList(int questionKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getAnswersForQuestions");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("questionKey={0}", questionKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return result;
        }

        //individualAnswer
        public async Task<string> getIndividualAnswer(int operationKey,int individualExtensionKey, int questionKey, int answerTemplateKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/individualAnswer");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("individualExtensionKey={0}", individualExtensionKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("questionKey={0}", questionKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("answerTemplateKey={0}", answerTemplateKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return result;
        }

        //getIndividualFAQAnswer
        public async Task<string> getIndividualFAQAnswer(int operationKey, int individualExtensionKey, int questionKey, int answerTemplateKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/individualAnswer");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("individualExtensionKey={0}", individualExtensionKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("questionKey={0}", questionKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("answerTemplateKey={0}", answerTemplateKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return result;
        }

        //getFAQ
        public async Task<string> fillFAQsList(int operationKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/getFAQ");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("operationKey={0}", operationKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return result;
        }

        //editIndividualsCallStatus
        public async Task<string> individualStatusChange(int IndividualExtensionKey, int CallStatusKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/editIndividualsCallStatus");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("IndividualExtensionKey={0}", IndividualExtensionKey);
                requestUriBuilder.Append("&");
                requestUriBuilder.AppendFormat("CallStatusKey={0}", CallStatusKey);

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return result;
        }

        //editIndividualsOrderNumber
        public async Task<string> individualOrderNumber(int IndividualExtensionKey)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Globals.Configuration.DATA_INTERFACE_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                StringBuilder requestUriBuilder = new StringBuilder();
                requestUriBuilder.Append("/CallCenter/editIndividualsOrderNumber");
                requestUriBuilder.Append("?");
                requestUriBuilder.AppendFormat("IndividualExtensionKey={0}", IndividualExtensionKey);
                

                HttpResponseMessage response = await client.GetAsync(requestUriBuilder.ToString()).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    string responseJSON = await response.Content.ReadAsStringAsync();

                    result = Convert.ToString(JsonConvert.DeserializeObject(responseJSON));

                    //ViewBag.CallStatusList = resultStatic.ToList();
                    //ViewBag.OperationList = result.Select(x => x.Value).ToList().Distinct();
                }
            }

            return result;
        }

        public ActionResult Questions()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult FAQs()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult StopWatch()
        {
            ViewBag.Message = "";

            return View();
        }
    }
}