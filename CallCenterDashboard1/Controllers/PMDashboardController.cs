﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CallCenterDashboard1.EDMX;
using CallCenterDashboard1.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Globalization;

namespace CallCenterDashboard1.Controllers
{
    public class PMDashboardController : Controller
    {
        public static string HashCode = "";

        [Authorize]
        // GET: PMDashboard
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult RequestService(string RequestKey, string AllowEdit = "", string individualid = null)
        {
            //individualid = "1001";
            var CallsIncoming = 0;
            var CallsOutGoing = 0;
            var VisitsIncoming = 0;
            var VisitsOutGoing = 0;
            var TotalNumberOfIndividuals = 0;

            RequestKey = RequestKey == null ? "" : RequestKey;
            individualid = individualid == null ? "" : individualid;

            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
            Dictionary<string, int> groupIndividualsCountDictionary = new Dictionary<string, int>();


            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                var userGroups = usersResult.Where(x => x.Email == User.Identity.Name).Select(x => x.Group).ToList();

                var userDashBoard = usersResult.First(x => x.UserName == User.Identity.Name).DashBoard;
                ViewBag.DashboardType = userDashBoard.Trim();

                List<GETservice_comm_Result> result2 = new List<GETservice_comm_Result>();
                result2 = PMSPEntities.GETservice_comm(null).ToList<GETservice_comm_Result>();
                ViewData["request"] = "";
                ViewData["Evaluation"] = "";
                //var users = result2.GroupBy(x => x.FromUser).Select(g => g.First());
                ViewData["service_comm"] = result2.Select(group => new SelectListItem { Text = group.comm_name, Value = group.comm_name.Trim() }).ToList();
                ServiceModel model = new ServiceModel();
                model.Request = new GetRequests_Result();
                model.Evaluation = new List<GETServiceEvaluation_Result>();
                model.MokademElTalab = model.MokademElTalab = new GetIndividualsByID_Result();
                model.AllowEdit = AllowEdit;
                string userID = string.Empty;

                List<GetUsers_Result> userList = new List<GetUsers_Result>();

                userList = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                userID = userList.First(x => x.UserName == User.Identity.Name).Id;


                try
                {
                    foreach (string groupName in userGroups)
                    {
                        var userHash = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name && x.Group == groupName).AssignedHash;
                        var groupIndividualsCount = Convert.ToInt32(PMSPEntities.CountIndividuals(userHash).ToList()[0]);

                        groupIndividualsCountDictionary.Add(groupName, groupIndividualsCount);
                    }
                }
                catch
                {
                    groupIndividualsCountDictionary.Add("No Group Available", 0);
                }

                List<GetRequestsDatatableFormByRequestType_Result> getRequestsCallsIncoming = new List<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsIncoming = PMSPEntities.GetRequestsDatatableFormByRequestType(null, "Call", null).ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsIncoming = getRequestsCallsIncoming.Where(x => x.RequestedFromUser == userID).ToList();
                getRequestsCallsIncoming = getRequestsCallsIncoming.OrderByDescending(x => x.RequestKey).ToList();
                CallsIncoming = getRequestsCallsIncoming.Count;

                List<GetRequestsDatatableFormByRequestType_Result> getRequestsCallsOutGoing = new List<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsOutGoing = PMSPEntities.GetRequestsDatatableFormByRequestType(null, "Call", null).ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsOutGoing = getRequestsCallsOutGoing.Where(x => x.RequestedCreatedByUser == userID).ToList().Where(x => x.notify == "0").ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsOutGoing = getRequestsCallsOutGoing.OrderByDescending(x => x.RequestKey).ToList();
                CallsOutGoing = getRequestsCallsOutGoing.Count;

                List<GetRequestsDatatableFormByRequestType_Result> getRequestsVisitsIncoming = new List<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsIncoming = PMSPEntities.GetRequestsDatatableFormByRequestType(null, "Visit", null).ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsIncoming = getRequestsVisitsIncoming.Where(x => x.RequestedFromUser == userID).ToList();
                getRequestsVisitsIncoming = getRequestsVisitsIncoming.OrderByDescending(x => x.RequestKey).ToList();
                VisitsIncoming = getRequestsVisitsIncoming.Count;

                List<GetRequestsDatatableFormByRequestType_Result> getRequestsVisitsOutGoing = new List<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsOutGoing = PMSPEntities.GetRequestsDatatableFormByRequestType(null, "Visit", null).ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsOutGoing = getRequestsVisitsOutGoing.Where(x => x.RequestedCreatedByUser == userID).ToList().Where(x => x.notify == "0").ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsOutGoing = getRequestsVisitsOutGoing.OrderByDescending(x => x.RequestKey).ToList();
                VisitsOutGoing = getRequestsVisitsOutGoing.Count;

                TotalNumberOfIndividuals = userGroups.Count;

                ViewBag.CallsIncoming = CallsIncoming;
                ViewBag.CallsOutGoing = CallsOutGoing;
                ViewBag.VisitsIncoming = VisitsIncoming;
                ViewBag.VisitsOutGoing = VisitsOutGoing;
                ViewBag.TotalNumberOfIndividuals = TotalNumberOfIndividuals;
                ViewBag.groupIndividualsCountDictionary = groupIndividualsCountDictionary;

                if (RequestKey != null)
                {
                    if (RequestKey != "")
                    {
                        model.Request = PMSPEntities.GetRequests(null, "1").ToList().Where(x => x.RequestKey == int.Parse(RequestKey)).FirstOrDefault();
                        model.Evaluation = PMSPEntities.GETServiceEvaluation(RequestKey).ToList();
                        model.MokademElTalab = PMSPEntities.GetIndividualsByID(model.Request.IndividualKey).FirstOrDefault();
                        model.TalebElKhidma = PMSPEntities.GetIndividualsByID(model.Request.AskForServiceKey).FirstOrDefault();
                        return View(model);
                    }
                }


                if (individualid != "")
                {
                    var individual = new GetIndividualsByID_Result();

                    individual = PMSPEntities.GetIndividualsByID(individualid).FirstOrDefault();

                    //ViewBag.Individual = Json(individual, JsonRequestBehavior.AllowGet);
                    var ttt = new JavaScriptSerializer().Serialize(individual);
                    ViewBag.Individual = new JavaScriptSerializer().Serialize(individual);
                }

            }


            return View();
        }

       

        public JsonResult CreateServiceRequest(int requestID, string requestedFromUser, string requestedCreatedByUser, string requestName, string requestDate, string requestSeen, string requestCompleted, string individualKey, string actionVisitInternalExternal, string actionTitle, string actionComment, string actionDueDate, string actionMaxDueDate, string communication, string introducer, string serviceDescription, string serviceReason, string note, string reminderDay, string reminderTime, string status, string comment, string requestVersion, string askForServiceKey, string askForServiceType, string serviceType, string serviceReference, string serviceLevel, string pointOfService, string priority, string notify, string isHost = null, string inviteeComment = null, string attendingStatus = null, string inviteeName = null, string individualsInvitees = null, string usersInvitees = null, string silatElTawasol = "")
        {
            List<string> usersToNotify = new List<string>();
            int requestKey = 0;
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                if (requestID == 0)
                {
                    var actions = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
                    AccountController acc = new AccountController();
                    List<GetUsers_Result> allServiceUsers = new List<GetUsers_Result>();
                    allServiceUsers = acc.GetServiceUsers().Where(x => x.DashBoard.Trim().ToLower() == "ServiceProvider".Trim().ToLower()).ToList();
                    int count = PMSPEntities.GetMou3arref(introducer).ToList().Where(x => x.Name == introducer).Count();
                    if (count == 0)
                    {
                        PMSPEntities.CreateMou3arref(introducer);
                    }
                    foreach (var user in allServiceUsers)
                    {
                        var result = PMSPEntities.CreateRequest(user.UserName, requestedFromUser, requestName, requestDate, requestSeen, requestCompleted, individualKey, actionVisitInternalExternal, actionTitle, actionComment, actionDueDate, actionMaxDueDate, communication, introducer, serviceDescription, serviceReason, note, reminderDay, reminderTime, status, comment, requestVersion, askForServiceKey, askForServiceType, serviceType, serviceReference, serviceLevel, pointOfService, priority, "1", isHost, inviteeComment, attendingStatus, inviteeName, null, null, silatElTawasol, "", null).ToList();
                        int.TryParse(result[0].ToString(), out requestKey);
                    }

                }
                else
                {
                    PMSPEntities.Database.ExecuteSqlCommand("Update [Requests] set [ActionComment] ='Update',[RequestDate]='" + requestDate + "'  where RequestKey=" + requestKey);
                }
                requestKey = requestID;
                PMSPEntities.Database.ExecuteSqlCommand("Delete From ServiceEvaluation where RequestKey='" + requestKey + "'");
            }
            return Json(requestKey, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NonVoter()
        {

            var TotalNumberOfIndividuals = 0;
            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
            Dictionary<string, int> groupIndividualsCountDictionary = new Dictionary<string, int>();
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                var userGroups = usersResult.Where(x => x.Email == User.Identity.Name).Select(x => x.Group).ToList();
                string userID = string.Empty;

                try
                {
                    foreach (string groupName in userGroups)
                    {
                        var userHash = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name && x.Group == groupName).AssignedHash;
                        var groupIndividualsCount = Convert.ToInt32(PMSPEntities.CountIndividuals(userHash).ToList()[0]);

                        groupIndividualsCountDictionary.Add(groupName, groupIndividualsCount);
                    }
                }
                catch
                {
                    groupIndividualsCountDictionary.Add("No Group Available", 0);
                }

                List<GetUsers_Result> userList = new List<GetUsers_Result>();

                userList = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                userID = userList.First(x => x.UserName == User.Identity.Name).Id;

                TotalNumberOfIndividuals = userGroups.Count;

            }
            return View();
        }

        public JsonResult getAllKadaa(string selectVariable = null, string whereVariable = null, string resultVariable = null)
        {
            whereVariable = whereVariable == "" ? null : whereVariable;
            resultVariable = resultVariable == "" ? null : resultVariable;
            selectVariable = selectVariable == "" ? null : selectVariable;
            List<string> result = new List<string>();

            //selectVariable = selectVariable.Replace("_", " ");
            //whereVariable = whereVariable.Replace("_", " ");
            //resultVariable = resultVariable.Replace("_", " ");

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                try
                {
                    result = PMSPEntities.GetFromIndividuals(whereVariable, selectVariable, resultVariable).ToList<string>();
                }
                catch
                {
                    result.Add("");
                }



            }
            result = result.OrderBy(x => x).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getFromIndividualsCombined(string Kadaa = null, string balda = null, string tayfe = null)
        {

            List<string> result = new List<string>();
            List<GetFromIndividualModel> items = new List<GetFromIndividualModel>();

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {

                if (balda == null || balda == "")
                {
                    result = PMSPEntities.Database.SqlQuery<string>("SELECT [رقم السجل] FROM [BeirutUI].[dbo].[Individual] where [الطائفة] = N'" + tayfe + "'  group by [رقم السجل]").ToList<string>();
                }

                else
                {
                    result = PMSPEntities.Database.SqlQuery<string>("SELECT [رقم السجل] FROM [BeirutUI].[dbo].[Individual] where   [بلدة النفوس] = N'" + balda + "' AND [الطائفة] = N'" + tayfe + "' group by [رقم السجل]").ToList<string>();
                }

            }

            foreach (var myItem in result)
            {
                GetFromIndividualModel item = new GetFromIndividualModel();
                item.Name = myItem;
                item.Value = myItem;
                items.Add(item);
            }
            items = items.OrderBy(x => x.Value).ToList();

            return Json(items.Select(x => x.Value).ToList<string>(), JsonRequestBehavior.AllowGet);
        }
        //بيروت الأولى
        public JsonResult getFromIndividuals(string selectVariable = null, string whereVariable = null, string resultVariable = null)
        {
            whereVariable = whereVariable == "" ? null : whereVariable;
            resultVariable = resultVariable == "" ? null : resultVariable;
            selectVariable = selectVariable == "" ? null : selectVariable;
            List<string> result = new List<string>();

            //selectVariable = selectVariable.Replace("_", " ");
            //whereVariable = whereVariable.Replace("_", " ");
            //resultVariable = resultVariable.Replace("_", " ");

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                try
                {
                    result = PMSPEntities.GetFromIndividuals(whereVariable, selectVariable, resultVariable).ToList<string>();
                    if(resultVariable == "بيروت الأولى")
                    {
                        result.Add("المدور");
                    }
                }
                catch
                {
                    result.Add("");
                }



            }

            if (selectVariable == "قضاء النفوس")
            {
                result = result.Where(x => x != null && (x.Equals("بيروت الأولى"))).ToList<string>();
            }

            if (selectVariable == "جمعية 1")
            {
                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    try
                    {
                        var newResult = PMSPEntities.GetFromIndividuals(null, "جمعية 2", null).ToList<string>();
                        var combinedResult = result.Union(newResult).ToList();
                        result = combinedResult;
                    }
                    catch
                    {
                        result.Add("");
                    }



                }

            }

            if (selectVariable == "جمعية 2")
            {
                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    try
                    {
                        var newResult = PMSPEntities.GetFromIndividuals(null, "جمعية 1", null).ToList<string>();
                        var combinedResult = result.Union(newResult).ToList();
                        result = combinedResult;
                    }
                    catch
                    {
                        result.Add("");
                    }

                }
            }

            result = result.OrderBy(x => x).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getALLIndividuals(string userHash = "", string pagination = "")
        {
            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
            List<GetIndividuals_Result> result = new List<GetIndividuals_Result>();
            string HashCode = "";
            if (userHash == "null")
                userHash = "";

            if (!userHash.Contains("1 = 1"))
            {
                HashCode = "1=1";
            }

            if (pagination == "")
            {
                pagination = "RN BETWEEN 1 AND 1000";
            }

            userHash = userHash.Replace("= N'(بيروت (الأولى'", "LIKE N'%الأولى%'");
            userHash = userHash.Replace("''", "'");
            userHash = userHash.Replace("_", " ");
            HashCode += userHash;
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();

                result = PMSPEntities.GetIndividuals(HashCode, pagination,"1").ToList<GetIndividuals_Result>();
            }

            result.ForEach(x => x.CheckBox = "");

            Dictionary<string, List<GetIndividuals_Result>> dictionnaryResult = new Dictionary<string, List<GetIndividuals_Result>>();

            dictionnaryResult.Add("data", result);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllServices()
        {
            List<string> services = new List<string>();
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                services = PMSPEntities.GetALLServices(null).ToList<string>();
            }
            return Json(services, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNonVoters()
        {

            List<GetNonVoters_Result> allNonVoters = new List<GetNonVoters_Result>();

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                allNonVoters = PMSPEntities.GetNonVoters().ToList();
            }

            Dictionary<string, List<GetNonVoters_Result>> allNonVotersResult = new Dictionary<string, List<GetNonVoters_Result>>();

            allNonVotersResult.Add("data",allNonVoters);

            return Json(allNonVotersResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetServices(string service)
        {
            List<string> services = new List<string>();
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                if (service == "All")
                {
                    services = PMSPEntities.GetServices(null).ToList();
                }
                else
                {
                    services = PMSPEntities.GetServices(service).ToList();
                }
            }

            return Json(services, JsonRequestBehavior.AllowGet);
        }

        public int gettotalnumberofrows(string HashCode = null, string FromGroup = null)
        {
            var individualnumber = 1;

            HashCode = HashCode == null ? HashCode : HashCode.Replace("\t", "");

            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
            List<GetIndividuals_Result> result = new List<GetIndividuals_Result>();
            string mHashCode = HashCode;
            if (HashCode != null)
            {
                HashCode = HashCode.Replace("''", "'");
                HashCode = HashCode.Replace("_", " ");
            }
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();

                if (FromGroup == "dashboard")
                {
                    if (HashCode == null)
                    {
                        if (HashCode.Contains("RN"))
                        {
                            HashCode = HashCode.Replace("AND RN", "/");
                            HashCode = HashCode.Split('/')[0];
                        }
                        else
                        {
                            HashCode = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name).AssignedHash;
                        }

                    }
                    else
                    {
                        HashCode = HashCode.Replace("''", "'");
                        HashCode = HashCode.Contains("1 = 1") ? HashCode : "1 = 1" + HashCode;
                    }
                }
                else
                {
                    if (HashCode == "")
                    {
                        HashCode = "1=1 ";
                    }
                    else
                    {
                        if (!HashCode.Contains("1 = 1"))
                            HashCode = "1=1 " + HashCode;
                    }
                }

                HashCode = HashCode.Replace("= N'(بيروت (الأولى'", "LIKE N'%الأولى%'");

                individualnumber = Convert.ToInt32(PMSPEntities.CountIndividuals(HashCode).ToList()[0]);
            }

            return individualnumber;
        }

        public ActionResult Individuals(string id)
        {
            var shi = id;
            ViewBag.IndividualID = id;

            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
            Dictionary<string, int> groupIndividualsCountDictionary = new Dictionary<string, int>();

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                var userGroups = usersResult.Where(x => x.Email == User.Identity.Name).Select(x => x.Group).ToList();

                try
                {
                    foreach (string groupName in userGroups)
                    {
                        var userHash = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name && x.Group == groupName).AssignedHash;
                        var groupIndividualsCount = Convert.ToInt32(PMSPEntities.CountIndividuals(userHash).ToList()[0]);

                        groupIndividualsCountDictionary.Add(groupName, groupIndividualsCount);
                    }
                }
                catch
                {
                    groupIndividualsCountDictionary.Add("No Group Available", 0);
                }

                string userID = string.Empty;

                List<GetUsers_Result> userList = new List<GetUsers_Result>();

                userList = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                userID = userList.First(x => x.UserName == User.Identity.Name).Id;

            }

            return View();
        }

        public ActionResult AddIndividual(string id)
        {
            if (id == "0" || id == null)
            {
                return PartialView("_AddNonVoter");
            }

            else
            {


                List<GetIndividualChildren_Result> childs = new List<GetIndividualChildren_Result>();
                ChildrensModels modal = new ChildrensModels();

                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    ViewData["Individual Management"] = PMSPEntities.GetActionByUsers(User.Identity.Name).Where(x => x.Actions.Contains("Individual Management")).ToList();
                    childs = PMSPEntities.GetIndividualChildren(id).ToList<GetIndividualChildren_Result>();
                    modal.allchildrens = childs;
                    //var json = new JavaScriptSerializer().Serialize(obj);
                    //ViewBag.Individual = PMSPEntities.GetIndividualsByID(id).FirstOrDefault();

                    var individual = new GetIndividualsByID_Result();

                    individual = PMSPEntities.GetIndividualsByID(id).FirstOrDefault();

                    //ViewBag.Individual = Json(individual, JsonRequestBehavior.AllowGet);
                    var ttt = new JavaScriptSerializer().Serialize(individual);
                    ViewBag.Individual = new JavaScriptSerializer().Serialize(individual);

                }
                return PartialView("_AddIndividual", modal);
            }

        }

        public JsonResult getNeighborhood(string balda = null)
        {

            List<Object> result = new List<object>();

            //selectVariable = selectVariable.Replace("_", " ");
            //whereVariable = whereVariable.Replace("_", " ");
            //resultVariable = resultVariable.Replace("_", " ");

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                try
                {
                    result = PMSPEntities.GetNeighborhood(balda).ToList<object>();
                }
                catch
                {
                    result.Add("");
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCountries()
        {
            List<GetCountries_Result> result = new List<GetCountries_Result>();

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                result = PMSPEntities.GetCountries().ToList<GetCountries_Result>();
            }

            var listResult = result.Where(x => x.countries_desc.ToLower() != "null").Select(x => x.countries_desc).ToList();

            return Json(listResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getEvaluation()
        {
            List<GetEvaluation_Result> result = new List<GetEvaluation_Result>();

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                result = PMSPEntities.GetEvaluation().ToList<GetEvaluation_Result>();
            }

            var listResult = result.Where(x => x.evaluation_desc.ToLower() != "null").Select(x => x.evaluation_desc).ToList();

            return Json(listResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getIndividualsDashBoardByHash(string userHash = null, string pagination = "")
        {
            List<GetIndividuals_Result> result = new List<GetIndividuals_Result>();
            userHash = userHash.Replace("''", "'");
            userHash = userHash.Replace("''", "'");
            //userHash = userHash.Substring(0, 5).ToLower().Contains("and") ? userHash.Remove(0, 4) : userHash;
            var hash = string.Empty;

            try
            {
                if (pagination == "")
                    pagination = "RN BETWEEN 1 AND 1000";
                if (!userHash.Contains("1 = 1"))
                    hash = Session["HashCode"].ToString() == "" ? "1=1 " : Session["HashCode"].ToString();
            }
            catch
            {
                hash = "1=1 ";
            }

            userHash = userHash.Replace("= N'(بيروت (الأولى'", "LIKE N'%الأولى%'");
            userHash = userHash.Replace("_", " ");
            userHash = userHash.Replace("'null'", "''");
            userHash = hash == "" ? userHash : hash + userHash;
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                result = PMSPEntities.GetIndividuals(userHash, pagination, "1").ToList<GetIndividuals_Result>().Take(1000).ToList();
            }

            result.ForEach(x => x.CheckBox = "");

            Dictionary<string, List<GetIndividuals_Result>> dictionnaryResult = new Dictionary<string, List<GetIndividuals_Result>>();

            dictionnaryResult.Add("data", result);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CreateRequest(string requestedCreatedByUser, string requestedFromUser,
            string ActionName, string requestDate, string requestSeen, string requestCompleted,
            string individualSelected, string actionVisitInternalExternal = null, string actionTitle = null,
            string actionComment = null, string actionDueDate = null, string actionMaxDueDate = null,
            string individualinvitees = null, string usersinvitees = null, string isHost = null,
            string status = null, string location = null, string nonVotersInvitees = null, string individualNotes = "", string generalNote = "")
        {
            int requestKey = 0;
            if (nonVotersInvitees == "")
            {
                nonVotersInvitees = null;
            }


            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                var result = PMSPEntities.CreateRequest(requestedFromUser, requestedCreatedByUser, ActionName, requestDate, requestSeen, requestCompleted, individualSelected, actionVisitInternalExternal, actionTitle, actionComment, actionDueDate, actionMaxDueDate, "", "", "", "", "", "", "", status, generalNote, "", "", "", "", "", "", "", "", "0", isHost, null, null, null, individualinvitees, usersinvitees, location, "", nonVotersInvitees).ToList();
                int.TryParse(result[0].ToString(), out requestKey);

            }

            if (ActionName == "Visit")
            {
                List<GetActionByUsers_Result> result2 = new List<GetActionByUsers_Result>();
                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    result2 = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
                }
                var indivNotes = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(individualNotes);
                if (requestKey != 0)
                {
                    foreach (var n in indivNotes)
                    {
                        using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                        {
                            var result = PMSPEntities.UpdateInvitees(requestKey.ToString(), n["ID"].ToString(), null, null, n["Note"].ToString());



                        }
                    }
                }


                result2 = result2.Where(x => x.Actions.Trim() == "Request a Visit" && (x.FromUser == requestedFromUser || x.ForwardToUser == requestedFromUser)).ToList<GetActionByUsers_Result>();
                result2 = result2.GroupBy(p => p.NotifyUser)
  .Select(g => g.First())
  .ToList();
                foreach (var user in result2)
                {
                    using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                    {

                        var result = PMSPEntities.CreateRequest(user.NotifyUser, requestedCreatedByUser, ActionName, requestDate, requestSeen, requestCompleted, individualSelected, actionVisitInternalExternal, actionTitle, actionComment, actionDueDate, actionMaxDueDate, "", "", "", "", "", "", "", status, generalNote, "", "", "", "", "", "", "", "", "Notify" + requestKey, isHost, null, null, null, individualinvitees, usersinvitees, location, "", nonVotersInvitees);


                    }
                }
            }

            if (ActionName == "Call")
            {

                List<GetActionByUsers_Result> result2 = new List<GetActionByUsers_Result>();
                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    result2 = PMSPEntities.GetActionByUsers(User.Identity.Name).ToList<GetActionByUsers_Result>();
                }

                result2 = result2.Where(x => x.Actions.Trim() == "Request a Call" && (x.FromUser == requestedFromUser || x.ForwardToUser == requestedFromUser)).ToList<GetActionByUsers_Result>();
                result2 = result2.GroupBy(p => p.NotifyUser).Select(g => g.First()).ToList();
                foreach (var user in result2)
                {
                    using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                    {
                        var result = PMSPEntities.CreateRequest(user.NotifyUser, requestedCreatedByUser, ActionName, requestDate, requestSeen, requestCompleted, individualSelected, actionVisitInternalExternal, actionTitle, actionComment, actionDueDate, actionMaxDueDate, "", "", "", "", "", "", "", status, "", "", "", "", "", "", "", "", "", "Notify" + requestKey, null, null, null, null, individualinvitees, usersinvitees, "", "", nonVotersInvitees);
                    }
                }
            }


            return Json("success", JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetGroupName(string individualID, string Field)
        {

            //updateHash = updateHash.Replace("N", "");
            List<GetGroupNames_Result> result = new List<GetGroupNames_Result>();
            List<string> allDataOwners = new List<string>();
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                result = PMSPEntities.GetGroupNames(individualID).ToList<GetGroupNames_Result>();
                foreach (var item in result)
                {
                    var groups = PMSPEntities.GETOwnerFields(null).ToList<GETOwnerFields_Result>().Where(x => x.Group == item.GroupName).ToList<GETOwnerFields_Result>();


                    groups = groups.Where(x => x.OwnFields == Field.Replace("_", " ") || x.OwnFields.Contains(Field.Replace("_", " "))).ToList();
                    allDataOwners.AddRange(groups.Select(x => x.UserName).ToList());
                }

            }
            allDataOwners = allDataOwners.Distinct().ToList();
            return Json(allDataOwners, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateChildrenIndividual(string individualID, List<string> ChildrenNames, List<string> ChildrenBirthDate, List<string> ChildrenSex, List<string> ChildrenAge, List<string> ChildStudy, List<string> Hobbies, List<string> ChildrenAssign)
        {

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                var result = PMSPEntities.DeleteIndividualChildren(individualID.Trim());
            }

            if (ChildrenNames != null)
            {
                for (int i = 0; i < ChildrenNames.Count; i++)
                {

                    using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                    {
                        var result = PMSPEntities.CreateChildren(ChildrenNames[i], ChildrenSex[i], ChildrenBirthDate[i], individualID.Trim(), ChildrenAge[i], ChildrenAssign[i], Hobbies[i], ChildStudy[i]);
                    }

                }
            }


            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetIndividualFamily(string individualid)
        {
            Dictionary<string, List<GetIndividuals_Result>> dictionnaryResult = new Dictionary<string, List<GetIndividuals_Result>>();

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {

                var individual = new GetIndividualsByID_Result();
                individual = PMSPEntities.GetIndividualsByID(individualid).FirstOrDefault();

                var userHash = " AND  [بلدة النفوس] = N'" + individual.بلدة_النفوس + "' AND [قضاء النفوس] = N'" + individual.قضاء_النفوس + "' AND [رقم السجل] = N'" + individual.رقم_السجل + "' AND [الطائفة] = N'" + individual.الطائفة + "'";

                List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
                List<GetIndividuals_Result> result = new List<GetIndividuals_Result>();
                string HashCode = "";
                if (userHash == "null")
                    userHash = "";

                if (!userHash.Contains("1 = 1"))
                {
                    HashCode = "1=1";
                }
                string pagination = "RN BETWEEN 1 AND 1000";


                userHash = userHash.Replace("= N'(بيروت (الأولى'", "LIKE N'%الأولى%'");
                userHash = userHash.Replace("''", "'");
                userHash = userHash.Replace("_", " ");
                HashCode += userHash;

                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();

                result = PMSPEntities.GetIndividuals(HashCode, pagination, "1").ToList<GetIndividuals_Result>();

                result.ForEach(x => x.CheckBox = "");
                dictionnaryResult.Add("data", result);
            }

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }





        public int GetIndividualFamilycount(string individualid)
        {
            Dictionary<string, List<GetIndividuals_Result>> dictionnaryResult = new Dictionary<string, List<GetIndividuals_Result>>();
            int count = 0;
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {

                var individual = new GetIndividualsByID_Result();
                individual = PMSPEntities.GetIndividualsByID(individualid).FirstOrDefault();

                var userHash = " AND  [بلدة النفوس] = N'" + individual.بلدة_النفوس + "' AND [قضاء النفوس] = N'" + individual.قضاء_النفوس + "' AND [رقم السجل] = N'" + individual.رقم_السجل + "' AND [الطائفة] = N'" + individual.الطائفة + "'";

                List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
                List<GetIndividuals_Result> result = new List<GetIndividuals_Result>();
                string HashCode = "";
                if (userHash == "null")
                    userHash = "";

                if (!userHash.Contains("1 = 1"))
                {
                    HashCode = "1=1";
                }
                string pagination = "RN BETWEEN 1 AND 1000";


                userHash = userHash.Replace("= N'(بيروت (الأولى'", "LIKE N'%الأولى%'");
                userHash = userHash.Replace("''", "'");
                userHash = userHash.Replace("_", " ");
                HashCode += userHash;

                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();

                result = PMSPEntities.GetIndividuals(HashCode, pagination, "1").ToList<GetIndividuals_Result>();
                 count = result.Count();
                result.ForEach(x => x.CheckBox = "");
                dictionnaryResult.Add("data", result);
            }

            return count;
        }

        public ActionResult IndividualTable(string id)
        {
            ViewBag.IndividualID = id;
            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();

            Dictionary<string, int> groupIndividualsCountDictionary = new Dictionary<string, int>();

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                var userGroups = usersResult.Where(x => x.Email == User.Identity.Name).Select(x => x.Group).ToList();
                try
                {
                    if (userGroups.Count > 0)
                    {
                        foreach (string groupName in userGroups)
                        {
                            var userHash = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name && x.Group == groupName).AssignedHash;
                            var groupIndividualsCount = Convert.ToInt32(PMSPEntities.CountIndividuals(userHash).ToList()[0]);

                            groupIndividualsCountDictionary.Add(groupName, groupIndividualsCount);
                        }
                    }
                    else
                    {
                        groupIndividualsCountDictionary.Add("No Group Available", 0);

                    }

                }
                catch
                {
                    groupIndividualsCountDictionary.Add("No Group Available", 0);
                }

                List<GetActionByUsers_Result> result2 = new List<GetActionByUsers_Result>();
                result2 = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
                result2 = result2.Where(x => x.Actions.Trim() == "Request a Call" && x.UserKey == User.Identity.Name).ToList<GetActionByUsers_Result>();
                var users = result2.GroupBy(x => x.FromUser).Select(g => g.First());

                ViewData["callusers"] = users.Select(group => new SelectListItem { Text = group.FromUser.Trim(), Value = group.FromUser.Trim() }).ToList();
                
                string userID = string.Empty;

                List<GetUsers_Result> userList = new List<GetUsers_Result>();

                userList = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                userID = userList.First(x => x.UserName == User.Identity.Name).Id;
            }



            return View();

            //return View();
        }

        public ActionResult IndividualsPartialView()
        {
            Session["HashCode"] = "";
            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                var userGroups = usersResult.Where(x => x.UserName == User.Identity.Name).Select(x => x.Group).ToList();
                try
                {
                    ViewData["userGroups"] = userGroups.Select(group => new SelectListItem { Text = group.Trim(), Value = group.Trim() }).ToList();
                }
                catch
                {
                    ViewData["userGroups"] = userGroups.Select(group => new SelectListItem { Text = "No Group Available", Value = "No Group Available" }).ToList();
                }
            }
            return PartialView();
        }

        public JsonResult kadanoufousdropdown(string selectVariable = null, string whereVariable = null, string resultVariable = null)
        {
            whereVariable = whereVariable == "" ? null : whereVariable;
            resultVariable = resultVariable == "" ? null : resultVariable;
            selectVariable = selectVariable == "" ? null : selectVariable;
            List<string> result = new List<string>();
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                try
                {
                    result = PMSPEntities.GetFromIndividuals(whereVariable, selectVariable, resultVariable).ToList<string>();
                }
                catch
                {
                    result.Add("");
                }
            }

            if (selectVariable == "قضاء النفوس")
            {
                result = result.Where(x => x != null && (x.Equals("بيروت الأولى"))).ToList<string>();
                //result = result.Where(x => x != null && x.Contains("بيروت")).ToList<string>();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult getHashByGroupName(string groupName = null)
        {
            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
            var userHash = string.Empty;

            groupName = groupName == "null" ? null : groupName;

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();

                if (groupName != null)
                {
                    userHash = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name && x.Group == groupName).AssignedHash;
                }
                else
                {

                    userHash = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name).AssignedHash;
                }
                userHash = userHash.Replace("_", " ");


            }

            return Json(userHash, JsonRequestBehavior.AllowGet);
        }

        public JsonResult quickUpdateIndividual(string updateHash, string individualID, string requestedCreatedByUser, string requestDate, string newdata = null, string olddata = null)
        {
            List<string> items = new List<string>();
            var list = new List<string>() { "قضاء السكن", "بلدة السكن", "الحي", "التصويت", "الوضع الانتخابي", "الانتماء السياسي", "منخب", "مفتاح", "الهاتف", "الخليوي" };
            foreach (var dataowner in list)
            {
                if (updateHash.Contains(dataowner))
                {
                    items.Add(dataowner);
                    using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                    {
                        List<GetGroupNames_Result> allGroups = new List<GetGroupNames_Result>();
                        List<GETOwnerFields_Result> allDataOwners = new List<GETOwnerFields_Result>();

                        allGroups = PMSPEntities.GetGroupNames(individualID).ToList<GetGroupNames_Result>();

                        foreach (var item in allGroups)
                        {
                            var groups = PMSPEntities.GETOwnerFields(null).ToList<GETOwnerFields_Result>().Where(x => x.Group == item.GroupName).ToList<GETOwnerFields_Result>();
                            groups = groups.Where(x => x.OwnFields == dataowner).ToList();
                            allDataOwners.AddRange(groups);
                        }
                        allDataOwners = allDataOwners.GroupBy(x => x.UserName).Select(grp => grp.First()).ToList();

                        foreach (var group in allDataOwners)
                        {
                            var result = PMSPEntities.CreateRequest(group.UserName, requestedCreatedByUser, "EditIndividual", requestDate, "0", "0", individualID, null, requestedCreatedByUser + "wants to edit " + dataowner + "  FROM " + olddata + " TO " + newdata, updateHash.Replace("N'", "'").Replace("'", ""), null, null, "", "", "", "", "", "", "", null, "", "", "", "", "", "", "", "", "", "0", null, null, null, null, null, null, null, "", null).ToList();
                        }

                        if (allDataOwners.Count < 1)
                        {
                            updateHash = updateHash.Replace("_", " ");
                            //updateHash = updateHash.Replace("N", "");
                            updateHash = updateHash.Replace("null", "'null'");
                            updateHash = updateHash.Replace("''null''", "'null'");
                            updateHash = updateHash.Replace("[عدد الخدمات] = N'null'", "[عدد الخدمات] = null");
                            var result = PMSPEntities.UpdateIndividual(updateHash, individualID, null);
                        }
                    }
                }
            }

            if (items.Count < 1)
            {
                updateHash = updateHash.Replace("_", " ");
                //updateHash = updateHash.Replace("N", "");
                updateHash = updateHash.Replace("null", "'null'");
                updateHash = updateHash.Replace("''null''", "'null'");
                updateHash = updateHash.Replace("[عدد الخدمات] = N'null'", "[عدد الخدمات] = null");

                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    var result = PMSPEntities.UpdateIndividual(updateHash, individualID, null);
                }

            }


            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult getIndividuals(string groupName = null)
        {
            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
            List<GetIndividuals_Result> result = new List<GetIndividuals_Result>();
            var userHash = string.Empty;

            groupName = groupName == "null" ? null : groupName;

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();

                if (groupName != null)
                {
                    userHash = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name && x.Group == groupName).AssignedHash;
                }
                else
                {

                    userHash = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name).AssignedHash;
                }

                userHash = userHash.Replace("= N'(بيروت (الأولى'", "LIKE N'%الأولى%'");
                userHash = userHash.Replace("_", " ");
                ViewData["HashCode"] = userHash;
                Session["HashCode"] = userHash;
                HashCode = userHash;
                result = PMSPEntities.GetIndividuals(userHash, "RN BETWEEN 1 AND 1000", "1").ToList<GetIndividuals_Result>().Take(1000).ToList();
            }

            result.ForEach(x => x.CheckBox = "");

            Dictionary<string, List<GetIndividuals_Result>> dictionnaryResult = new Dictionary<string, List<GetIndividuals_Result>>();

            dictionnaryResult.Add("data", result);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public JsonResult GetIndividualByID(string IndividualKey)
        {
            var Indinvidual = new GetIndividualsByID_Result();
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                Indinvidual = PMSPEntities.GetIndividualsByID(IndividualKey).FirstOrDefault();
            }
            return Json(Indinvidual, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult RequestVisit(string[] ids, string RequestCompleted = null, string ActionVisitInternalExternal = null, string ActionTitle = null,
           string ActionComment = null, string ActionDueDate = null, string ActionMaxDueDate = null, string RequestKey = null, string Notify = null, string Status = null, string Location = null, string SuggestedDate = null, string generalNote = null, string RequestedCreatedByUser = null, string ActionName = null)
        {
            ViewBag.RequestCompleted = RequestCompleted;
            ViewBag.ActionVisitInternalExternal = ActionVisitInternalExternal;
            ViewBag.ActionTitle = ActionTitle;
            ViewBag.ActionComment = ActionComment;
            ViewBag.ActionDueDate = ActionDueDate;
            ViewBag.ActionMaxDueDate = ActionMaxDueDate;

            ViewBag.RequestKey = RequestKey;
            ViewBag.Notify = Notify;
            ViewBag.Status = Status;
            ViewBag.Location = Location;
            ViewBag.SuggestedDate = SuggestedDate;
            ViewBag.generalNote = generalNote;
            ViewBag.RequestedCreatedByUser = "";
            ViewBag.RequestedFromUser = "";
            List<GetRequestsDatatableFormByRequestType_Result> getRequests = new List<GetRequestsDatatableFormByRequestType_Result>();
            if (Notify != "0" && Notify != "Notify" && RequestKey != null)
            {
                //getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user, "Approve Visit", requestID).ToList<GetRequestsDatatableFormByRequestType_Result>();
                //if (getRequests[0].notify != "0" || getRequests[0].notify != "Notify")
                //{
                //    string mNotify = getRequests[0].notify;
                //    getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(getRequests[0].RequestedCreatedByUser, "Approve Visit", getRequests[0].notify.Split('y')[1]).ToList<GetRequestsDatatableFormByRequestType_Result>();
                //    getRequests[0].notify = mNotify;
                //    //var requests = PMSPEntities.GetRequests.
                //}
                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    string mNotify = "";
                    if (ActionName == "Approve Visit")
                    {
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(User.Identity.Name, "Approve Visit", RequestKey).ToList<GetRequestsDatatableFormByRequestType_Result>();
                        mNotify = getRequests[0].notify;
                        var user = PMSPEntities.GetUsers(null).Where(x => x.Id == getRequests[0].RequestedCreatedByUser).FirstOrDefault();
                        var request = PMSPEntities.GetRequests(null, null).Where(x => x.RequestKey == int.Parse(getRequests[0].notify.Split('y')[1])).FirstOrDefault();
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user.UserName, "Visit", getRequests[0].notify.Split('y')[1]).ToList<GetRequestsDatatableFormByRequestType_Result>();
                    }
                    else
                    {
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(User.Identity.Name, "Approve Visit", RequestKey).ToList<GetRequestsDatatableFormByRequestType_Result>();
                    }



                    ViewBag.RequestCompleted = getRequests[0].RequestCompleted;
                    ViewBag.ActionVisitInternalExternal = getRequests[0].ActionVisitInternalExternal;
                    ViewBag.ActionTitle = getRequests[0].ActionTitle;
                    ViewBag.ActionComment = getRequests[0].ActionComment;
                    ViewBag.ActionDueDate = getRequests[0].ActionDueDate;
                    ViewBag.ActionMaxDueDate = getRequests[0].ActionMaxDueDate;

                    ViewBag.RequestKey = getRequests[0].RequestKey;

                    ViewBag.Status = getRequests[0].Status;
                    ViewBag.Location = getRequests[0].Location;
                    ViewBag.SuggestedDate = getRequests[0].ApprovedDate;
                    ViewBag.generalNote = getRequests[0].Comment;
                    ViewBag.RequestedCreatedByUser = "";
                    ViewBag.RequestedFromUser = "";
                    getRequests[0].notify = mNotify;
                    //    //var requests = PMSPEntities.GetRequests.
                }

            }


            var CallsIncoming = 0;
            var CallsOutGoing = 0;
            var VisitsIncoming = 0;
            var VisitsOutGoing = 0;
            var TotalNumberOfIndividuals = 0;

            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();
            Dictionary<string, int> groupIndividualsCountDictionary = new Dictionary<string, int>();


            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                var userGroups = usersResult.Where(x => x.Email == User.Identity.Name).Select(x => x.Group).ToList();
                if (RequestKey != null)
                {
                    var request = PMSPEntities.GetRequests(null, null).ToList().Where(x => x.RequestKey == int.Parse(RequestKey)).FirstOrDefault();
                    ViewBag.RequestedCreatedByUser = request.RequestedCreatedByUser;
                    ViewBag.RequestedFromUser = request.RequestedFromUser;
                }


                ViewData["allUsers"] = usersResult.Select(user => new SelectListItem { Text = user.UserName.Trim(), Value = user.UserName.Trim() }).ToList();
                ViewData["managerUsers"] = usersResult.Where(x => x.UserType != "User").Select(user => new SelectListItem { Text = user.UserName.Trim(), Value = user.UserName.Trim() }).ToList();



                List<GetActionByUsers_Result> result3 = new List<GetActionByUsers_Result>();
                result3 = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
                result3 = result3.Where(x => x.Actions.Trim() == "Request a Call" && x.UserKey == User.Identity.Name).ToList<GetActionByUsers_Result>();
                var userss = result3.GroupBy(x => x.FromUser).Select(g => g.First());

                ViewData["callusers"] = userss.Select(group => new SelectListItem { Text = group.FromUser.Trim(), Value = group.FromUser.Trim() }).ToList();






                try
                {
                    foreach (string groupName in userGroups)
                    {
                        var userHash = "1 = 1" + usersResult.First(x => x.UserName == User.Identity.Name && x.Group == groupName).AssignedHash;
                        var groupIndividualsCount = Convert.ToInt32(PMSPEntities.CountIndividuals(userHash).ToList()[0]);

                        groupIndividualsCountDictionary.Add(groupName, groupIndividualsCount);
                    }
                }
                catch
                {
                    groupIndividualsCountDictionary.Add("No Group Available", 0);
                }

                List<GetActionByUsers_Result> result2 = new List<GetActionByUsers_Result>();
                result2 = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
                result2 = result2.Where(x => x.Actions.Trim() == "Request a Visit" && x.UserKey == User.Identity.Name && x.FromUser != null).ToList<GetActionByUsers_Result>();
                var users = result2.GroupBy(x => x.FromUser).Select(g => g.First());
                ViewData["visitsusers"] = users.Select(group => new SelectListItem { Text = group.FromUser.Trim(), Value = group.FromUser.Trim() }).ToList();
                string userID = string.Empty;

                List<GetUsers_Result> userList = new List<GetUsers_Result>();

                userList = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                userID = userList.First(x => x.UserName == User.Identity.Name).Id;


                List<GetRequestsDatatableFormByRequestType_Result> getRequestsCallsIncoming = new List<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsIncoming = PMSPEntities.GetRequestsDatatableFormByRequestType(null, "Call", null).ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsIncoming = getRequestsCallsIncoming.Where(x => x.RequestedFromUser == userID).ToList();
                getRequestsCallsIncoming = getRequestsCallsIncoming.OrderByDescending(x => x.RequestKey).ToList();
                CallsIncoming = getRequestsCallsIncoming.Count;

                List<GetRequestsDatatableFormByRequestType_Result> getRequestsCallsOutGoing = new List<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsOutGoing = PMSPEntities.GetRequestsDatatableFormByRequestType(null, "Call", null).ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsOutGoing = getRequestsCallsOutGoing.Where(x => x.RequestedCreatedByUser == userID).ToList().Where(x => x.notify == "0").ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsCallsOutGoing = getRequestsCallsOutGoing.OrderByDescending(x => x.RequestKey).ToList();
                CallsOutGoing = getRequestsCallsOutGoing.Count;

                List<GetRequestsDatatableFormByRequestType_Result> getRequestsVisitsIncoming = new List<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsIncoming = PMSPEntities.GetRequestsDatatableFormByRequestType(null, "Visit", null).ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsIncoming = getRequestsVisitsIncoming.Where(x => x.RequestedFromUser == userID).ToList();
                getRequestsVisitsIncoming = getRequestsVisitsIncoming.OrderByDescending(x => x.RequestKey).ToList();
                VisitsIncoming = getRequestsVisitsIncoming.Count;

                List<GetRequestsDatatableFormByRequestType_Result> getRequestsVisitsOutGoing = new List<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsOutGoing = PMSPEntities.GetRequestsDatatableFormByRequestType(null, "Visit", null).ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsOutGoing = getRequestsVisitsOutGoing.Where(x => x.RequestedCreatedByUser == userID).ToList().Where(x => x.notify == "0").ToList<GetRequestsDatatableFormByRequestType_Result>();
                getRequestsVisitsOutGoing = getRequestsVisitsOutGoing.OrderByDescending(x => x.RequestKey).ToList();
                VisitsOutGoing = getRequestsVisitsOutGoing.Count;

                TotalNumberOfIndividuals = userGroups.Count;
            }

            ViewBag.CallsIncoming = CallsIncoming;
            ViewBag.CallsOutGoing = CallsOutGoing;
            ViewBag.VisitsIncoming = VisitsIncoming;
            ViewBag.VisitsOutGoing = VisitsOutGoing;
            ViewBag.TotalNumberOfIndividuals = TotalNumberOfIndividuals;
            ViewBag.groupIndividualsCountDictionary = groupIndividualsCountDictionary;

            return View();
        }


        public JsonResult getUsers()
        {
            List<GetUsers_Result> usersResult = new List<GetUsers_Result>();

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                usersResult = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
            }
            usersResult = usersResult.GroupBy(x => x.UserName).Select(grp => grp.First()).ToList();

            Dictionary<string, List<GetUsers_Result>> dictionnaryResult = new Dictionary<string, List<GetUsers_Result>>();

            dictionnaryResult.Add("data", usersResult);

            return Json(dictionnaryResult, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetActionsByType(string type)
        {
            var username = User.Identity.Name;
            List<string> result = new List<string>();
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                var result2 = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
                result = result2.Where(x => x.Actions.Trim() == "Request a Visit" && x.UserKey == User.Identity.Name && (x.Description == type.ToLower() || x.Description == "all")).Select(x => x.FromUser).ToList<string>();
                // result = result2.Where(x => (x.Actions.Trim() == "Request a Visit" && x.UserKey == User.Identity.Name && x.Description == type && x.FromUser != null) || x.Description == "all").Select(x => x.FromUser).ToList<string>();

            }
            result = result.Distinct().ToList();


            return Json(result, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public JsonResult viewAllNotifications(string user)
        {
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                var result = PMSPEntities.ViewedAllNotifications(user, null, null, null, null, null, null, null, "1");
            }

            return Json("success", JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public JsonResult GetNotificationRequest(string user, string Time = null)
        {
            Time = Time.Replace(",", "");
            //try
            //{
                IFormatProvider culture = new CultureInfo("en-US", true);
            DateTime timenow = DateTime.ParseExact(Time, "dd/MM/yyyy HH:mm:ss", culture);
            //DateTime timenow = Convert.ToDateTime(Time);
                List<GetRequests_Result> getRequests = new List<GetRequests_Result>();

                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    getRequests = PMSPEntities.GetRequests(user, null).ToList<GetRequests_Result>();
                }

                getRequests = getRequests.Where(x => x.RequestName == "CallBackReminder" && x.ReminderDay != "").ToList<GetRequests_Result>();


                getRequests = getRequests.Where(x => Convert.ToDateTime(x.ReminderDay + " " + x.ReminderTime) <= timenow).ToList<GetRequests_Result>();


                getRequests = getRequests.OrderByDescending(x => x.RequestKey).ToList();
                //var requests = getRequests.Take(20);

                return Json(getRequests, JsonRequestBehavior.AllowGet);
            //}
            //catch
            //{
            //    return Json("Error", JsonRequestBehavior.AllowGet);
            //}


        }




        [HttpGet]
        public JsonResult GetNotificationRequestTable(string ActionName, string user = null, string requestID = null, string time = null)
        {
            string userID = string.Empty;

            List<GetUsers_Result> userList = new List<GetUsers_Result>();

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                userList = PMSPEntities.GetUsers(null).ToList<GetUsers_Result>();
                userID = userList.First(x => x.UserName == User.Identity.Name).Id;
            }

            List<GetRequestsDatatableFormByRequestType_Result> getRequests = new List<GetRequestsDatatableFormByRequestType_Result>();
            AccountController acc = new AccountController();
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                switch (ActionName.Trim())
                {
                    case "WrongNumber":
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user, "WrongNumber", requestID).ToList<GetRequestsDatatableFormByRequestType_Result>();
                        getRequests = getRequests.Where(x => x.RequestedFromUser == userID).ToList();
                        foreach (var request in getRequests)
                        {
                            ViewBag.RequestCompleted = request.RequestCompleted;
                        }
                        break;

                    case "Call":
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user, "Call", requestID).ToList<GetRequestsDatatableFormByRequestType_Result>();
                        getRequests = getRequests.Where(x => x.RequestedFromUser == userID).ToList();
                        break;

                    case "Visit":
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user, "Visit", requestID).ToList<GetRequestsDatatableFormByRequestType_Result>();
                        break;

                    case "Approve Visit":
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user, "Approve Visit", requestID).ToList<GetRequestsDatatableFormByRequestType_Result>();
                        break;

                    case "IncomingCalls":
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user, "Call", requestID).ToList<GetRequestsDatatableFormByRequestType_Result>();
                        getRequests = getRequests.Where(x => x.RequestedFromUser == userID).ToList();
                        getRequests = getRequests.OrderByDescending(x => x.RequestKey).ToList();

                        foreach (var request in getRequests)
                        {
                            var Individual = PMSPEntities.GetIndividuals("1=1 AND ID='" + request.IndividualKey + "'", "RN BETWEEN 1 AND 1000", "1").FirstOrDefault();
                            request.IndividualFullName = Individual.الاسم + " " + Individual.اسم_الأب + " " + Individual.العائلة;
                            request.intimaa = Individual.التصويت + " / " + Individual.الانتماء_السياسي + " / " + Individual.الانتماء_السياسي_group_;
                            request.baldaTayfehSejel = Individual.رقم_السجل + " / " + Individual.الطائفة + " / " + Individual.بلدة_النفوس;
                            request.UserName = userList.First(x => x.Id == request.RequestedCreatedByUser.Trim()).Email;
                        }
                        break;

                    case "OutGoingCalls":
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user, "Call", requestID).ToList<GetRequestsDatatableFormByRequestType_Result>();
                        getRequests = getRequests.Where(x => x.RequestedCreatedByUser == userID).ToList().Where(x => x.notify == "0").ToList<GetRequestsDatatableFormByRequestType_Result>();
                        getRequests = getRequests.OrderByDescending(x => x.RequestKey).ToList();
                        foreach (var request in getRequests)
                        {
                            var Individual = PMSPEntities.GetIndividuals("1=1 AND ID='" + request.IndividualKey + "'", "RN BETWEEN 1 AND 1000", "1").FirstOrDefault();
                            request.IndividualFullName = Individual.الاسم + " " + Individual.اسم_الأب + " " + Individual.الشهرة;
                            request.intimaa = Individual.التصويت + " / " + Individual.الانتماء_السياسي + " / " + Individual.الانتماء_السياسي_group_;
                            request.baldaTayfehSejel = Individual.رقم_السجل + " / " + Individual.الطائفة + " / " + Individual.بلدة_النفوس;
                        }
                        break;

                    case "IncomingVisits":
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user, "Visit", requestID).ToList<GetRequestsDatatableFormByRequestType_Result>();
                        getRequests = getRequests.Where(x => x.RequestedFromUser == userID).ToList();
                        getRequests = getRequests.OrderByDescending(x => x.RequestKey).ToList();
                        foreach (var request in getRequests)
                        {
                            var Individual = PMSPEntities.GetIndividuals("1=1 AND ID='" + request.IndividualKey + "'", "RN BETWEEN 1 AND 1000", "1").FirstOrDefault();
                            request.IndividualFullName = "";
                            request.IndividualFullName = Individual.الاسم + " " + Individual.اسم_الأب + " " + Individual.الشهرة;
                            request.intimaa = Individual.التصويت + " / " + Individual.الانتماء_السياسي + " / " + Individual.الانتماء_السياسي_group_;
                            request.baldaTayfehSejel = Individual.رقم_السجل + " / " + Individual.الطائفة + " / " + Individual.بلدة_النفوس;
                            request.UserName = userList.First(x => x.Id == request.RequestedCreatedByUser).Email;
                            //request.UserName = "";
                            //var applicationUser = new ApplicationUser();
                            //applicationUser = acc.GetUserByID(request.RequestedCreatedByUser);
                            //request.UserName = applicationUser.UserName;
                        }
                        break;

                    case "OutGoingVisits":
                        getRequests = PMSPEntities.GetRequestsDatatableFormByRequestType(user, "Visit", requestID).ToList<GetRequestsDatatableFormByRequestType_Result>();
                        getRequests = getRequests.Where(x => x.RequestedCreatedByUser == userID).ToList().Where(x => x.notify == "0").ToList<GetRequestsDatatableFormByRequestType_Result>();
                        getRequests = getRequests.OrderByDescending(x => x.RequestKey).ToList();
                        foreach (var request in getRequests)
                        {
                            var Individual = PMSPEntities.GetIndividuals("1=1 AND ID='" + request.IndividualKey + "'", "RN BETWEEN 1 AND 1000", "1").FirstOrDefault();
                            request.IndividualFullName = "";
                            request.IndividualFullName = Individual.الاسم + " " + Individual.اسم_الأب + " " + Individual.الشهرة;
                            request.intimaa = Individual.التصويت + " / " + Individual.الانتماء_السياسي + " / " + Individual.الانتماء_السياسي_group_;
                            request.baldaTayfehSejel = Individual.رقم_السجل + " / " + Individual.الطائفة + " / " + Individual.بلدة_النفوس;
                        }
                        break;
                }
            }
            getRequests.OrderBy(x => x.RequestDate);

            Dictionary<string, List<GetRequestsDatatableFormByRequestType_Result>> dictionnaryGetRequests = new Dictionary<string, List<GetRequestsDatatableFormByRequestType_Result>>();

            dictionnaryGetRequests.Add("data", getRequests);

            return Json(dictionnaryGetRequests, JsonRequestBehavior.AllowGet);

        }





        [HttpGet]
        public string createreminder(string requestedCreatedByUser, string individualid, string status,string reminderday, string remindertime, string callbackpurpose,string requestDate)
        {

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                var result = PMSPEntities.CreateRequest(requestedCreatedByUser, requestedCreatedByUser, "CallBackReminder", requestDate, "0", "0", individualid, null, null, status, null, null, "", "", "", "", "", "", "", status, null, "", "", "", "", "", "", "", "", "0", null, null, null, null, null, null, null, "", null).ToList();
                var result1 = PMSPEntities.ViewedAllNotifications(null, status, callbackpurpose, reminderday, remindertime, "", callbackpurpose, result.First().ToString(), "0");
                //var result = PMSPEntities.ViewedAllNotifications(null, callpurpose, note, reminderdate, remindertime, callstatus, callcomment, requestkey, "0");

            }

            return "";
        }

        [HttpGet]
        public JsonResult ApproveEditIndividual(string columnsStatement, string individualID)
        {

            string query = "";
            if (columnsStatement.Contains("مفتاح"))
            {
                columnsStatement = columnsStatement.Replace("where", "/");
                string q1 = columnsStatement.Split('/')[0];
                query = q1.Split('=')[0].Replace("_", " ") + " = N'" + q1.Split('=')[1].Trim() + "'";
                string q2 = columnsStatement.Split('/')[1];
                var q2Result = q2.Replace("AND", "/").Split('/');
                string whereQuery = " ";
                foreach (var item in q2Result)
                {
                    whereQuery += item.Split('=')[0].Replace("_", " ") + " = N'" + item.Split('=')[1].Trim() + "' AND";
                }
                int index = whereQuery.LastIndexOf("AND");
                whereQuery = whereQuery.Substring(0, index);

                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    var result = PMSPEntities.UpdateIndividual(query, null, whereQuery);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                query = columnsStatement.Split('=')[0].Replace("_", " ") + " = N'" + columnsStatement.Split('=')[1].Trim() + "'";
                using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
                {
                    var result = PMSPEntities.UpdateIndividual(query, individualID, null);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
        }


        [HttpPost]
        public JsonResult UpdateIndividual(string updateHash, string individualID)
        {

            updateHash = updateHash.Replace("_", " ");
            //updateHash = updateHash.Replace("N", "");
            updateHash = updateHash.Replace("null", "'null'");
            updateHash = updateHash.Replace("''null''", "'null'");
            updateHash = updateHash.Replace("[عدد الخدمات] = N'null'", "[عدد الخدمات] = null");

            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                var result = PMSPEntities.UpdateIndividual(updateHash, individualID, null);
            }




            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}