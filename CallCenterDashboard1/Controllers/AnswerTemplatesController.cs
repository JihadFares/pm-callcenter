﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CallCenterDashboard1.EDMX;

namespace CallCenterDashboard1.Controllers
{
    public class AnswerTemplatesController : Controller
    {
        private PMCallCenterEntities db = new PMCallCenterEntities();

        // GET: AnswerTemplates
        public ActionResult Index()
        {
            var answerTemplates = db.AnswerTemplates.Include(a => a.Question);
            return View(answerTemplates.ToList());
        }

        // GET: AnswerTemplates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnswerTemplate answerTemplate = db.AnswerTemplates.Find(id);
            if (answerTemplate == null)
            {
                return HttpNotFound();
            }
            return View(answerTemplate);
        }

        // GET: AnswerTemplates/Create
        public ActionResult Create()
        {
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text");
            return View();
        }

        // POST: AnswerTemplates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AnswerTemplateKey,Value,QuestionKey")] AnswerTemplate answerTemplate)
        {
            if (ModelState.IsValid)
            {
                db.AnswerTemplates.Add(answerTemplate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text", answerTemplate.QuestionKey);
            return View(answerTemplate);
        }

        // GET: AnswerTemplates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnswerTemplate answerTemplate = db.AnswerTemplates.Find(id);
            if (answerTemplate == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text", answerTemplate.QuestionKey);
            return View(answerTemplate);
        }

        // POST: AnswerTemplates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AnswerTemplateKey,Value,QuestionKey")] AnswerTemplate answerTemplate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(answerTemplate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.QuestionKey = new SelectList(db.Questions, "QuestionKey", "Text", answerTemplate.QuestionKey);
            return View(answerTemplate);
        }

        // GET: AnswerTemplates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnswerTemplate answerTemplate = db.AnswerTemplates.Find(id);
            if (answerTemplate == null)
            {
                return HttpNotFound();
            }
            return View(answerTemplate);
        }

        // POST: AnswerTemplates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AnswerTemplate answerTemplate = db.AnswerTemplates.Find(id);
            db.AnswerTemplates.Remove(answerTemplate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
