﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CallCenterDashboard1.EDMX;
using CallCenterDashboard1.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
namespace CallCenterDashboard1.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                List<GetActionByUsers_Result> result2 = new List<GetActionByUsers_Result>();
                result2 = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
                result2 = result2.Where(x => x.Actions.Trim() == "Request a Call" && x.UserKey == User.Identity.Name).ToList<GetActionByUsers_Result>();
                var users = result2.GroupBy(x => x.FromUser).Select(g => g.First());
                ViewData["callusers"] = users.Select(group => new SelectListItem { Text = group.FromUser.Trim(), Value = group.FromUser.Trim() }).ToList();
                ViewBag.Message = "Your Dashboard page.";
            }

            return View("~/Views/Home/Dashboard.cshtml");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Dashboard()
        {

        
            using (BeirutUIEntities PMSPEntities = new BeirutUIEntities())
            {
                List<GetActionByUsers_Result> result2 = new List<GetActionByUsers_Result>();
                result2 = PMSPEntities.GetActionByUsers(null).ToList<GetActionByUsers_Result>();
                result2 = result2.Where(x => x.Actions.Trim() == "Request a Call" && x.UserKey == User.Identity.Name).ToList<GetActionByUsers_Result>();
                var users = result2.GroupBy(x => x.FromUser).Select(g => g.First());
                ViewData["callusers"] = users.Select(group => new SelectListItem { Text = group.FromUser.Trim(), Value = group.FromUser.Trim() }).ToList();
                ViewBag.Message = "Your Dashboard page.";

                return View();
            }

        
        
        }


    }
}