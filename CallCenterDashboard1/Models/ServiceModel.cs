﻿using CallCenterDashboard1.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallCenterDashboard1.Models
{
    public class ServiceModel
    {
        public GetRequests_Result Request { get; set; }
        public List<GETServiceEvaluation_Result> Evaluation { get; set; }
        public GetIndividualsByID_Result TalebElKhidma { get; set; }
        public GetIndividualsByID_Result MokademElTalab { get; set; }
        public string AllowEdit { get; set;}
    }
}