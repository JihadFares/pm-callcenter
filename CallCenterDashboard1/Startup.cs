﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CallCenterDashboard1.Startup))]
namespace CallCenterDashboard1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
